# Waya Africa Gateway Client API.

## Accessing the API
The documentation can be found here:
```
https://wayaafrica.com/docs
```

You may have to request for access to view the documentation and test the API.