var waya = require('../controllers/helper');
var vfy = require('../authentication/verify');
var express = require('express');
var route = express();
var bod = require('body-parser');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());

route.post('/conversion', vfy, function(req, res) {
    waya.postCall(JSON.stringify(req.body), `fx/conversion`, req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.post('/rates', vfy, function(req, res) {
    waya.postCall(JSON.stringify(req.body), `fx/rates`, req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});

module.exports = route;