var waya = require('../controllers/helper');
var vfy = require('../authentication/verify');
var express = require('express');
var route = express();
var bod = require('body-parser');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());

route.get('/', function(req, res) {
    waya.getCall('', 'currency/getCurrencies', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});

module.exports = route;