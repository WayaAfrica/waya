var db = require('./db');
var waya = require('../controllers/helper');
var express = require('express');
var app = express();
var comms = require('request');
var bod = require('body-parser');
app.use(bod.urlencoded({ extended: true }));
app.use(bod.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.header("Access-Control-Allow-Headers", "*");
    next();
});

app.get('/getToken', (req, res) => {
    var bearer = req.headers['authorization'];
    if (!bearer) return res.status(403).send({ success: false, message: 'No authorization key provided.' });
    let data = bearer.substr(bearer.indexOf(' ')).trim();
    let buf = Buffer.from(data, 'base64');
    let u_p = buf.toString('ascii');
    var un = u_p.substr(0, u_p.indexOf(':'));
    var pw = u_p.substr(u_p.indexOf(':') + 1);
    var data_ = JSON.stringify({'username':un, 'password':pw});

    serve(data_, 'auth/signin', true).then((resp) => {
        if (resp.status == 500 || resp.status == 403 || resp.status == 401 || !resp.token) {
          waya.talk(res, 401, { success:false, response: `${resp.message}. Invalid username or password`}, true);
          return false;
        }
        
        waya.talk(res, 200, { success:true, response:resp }, true);
    });
})

app.get('/transactionStatus/:trx_id', merchVerify, function(req, res) {
    serve('', `transaction/${req.params.trx_id}`, false, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

app.get('/searchTransaction', merchVerify, function(req, res) {
    serve('', `transaction/search?${req.query}`, false, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

app.post('/requestPayment/fromMomo', merchVerify, function(req, res) {
    serve('', 'users/me', false, req.headers['x-access-token']).then((resp) => {

        if (resp.status == 500 || resp.status == 403) {
            waya.talk(res, 401, { success:false, response: `An error occurred. ${resp.message}`}, true);
            return false;
        }

        var merch = resp.merchant.merchantID;
        req.body.merchantId = merch;

        serve(JSON.stringify(req.body), `transaction/payment`, true, req.headers['x-access-token']).then((resp) => {

            //Send Response to Client
            waya.out(res, resp, '');
        });
    });
});

app.post('/requestPayment/fromCard', merchVerify, function(req, res) {
    serve('', 'users/me', false, req.headers['x-access-token']).then((resp) => {

        if (resp.status == 500 || resp.status == 403) {
            waya.talk(res, 401, { success:false, response: `An error occurred. ${resp.message}`}, true);
            return false;
        }

        var merch = resp.merchant.merchantID;
        req.body.merchantId = merch;

        serve(JSON.stringify(req.body), `transaction/payment`, true, req.headers['x-access-token']).then((resp) => {

            //Send Response to Client
            waya.out(res, resp, '');
        });
    });
});

app.post('/transfer/toMomo', merchVerify, function(req, res) {
    if (req.body.destination.accountType !== 'MobileWallet') {
        waya.talk(res, 401, { success:false, response: `Destination account type not accurate`}, true);
        return false;
    }
    
    serve(JSON.stringify(req.body), `transaction/transfer`, req.query.live, true).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

app.post('/transfer/toBank', merchVerify, function(req, res) {
    if (req.body.destination.accountType !== 'BankAccount') {
        waya.talk(res, 401, { success:false, response: `Destination account type not accurate`}, true);
        return false;
    }
    serve(JSON.stringify(req.body), `transaction/transfer`, req.query.live, true).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});

function serve(data, endPoint, isPost = false, token) {
    options = {
        headers: { 'Content-Type':'application/json', 'Authorization': `Bearer ${token}` },
        url: process.env.BASE + endPoint,
        body: data,
    };
    
    return new Promise(function(resolve, reject) {
        if (isPost) {
            comms.post(options, (err, res, body_) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`P_ResponseBody: ${body_}`);
                    resolve(JSON.parse(body_));
                }
            });
        } else {
            comms.get(options, (err, res, body_) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`G_ResponseBody: ${body_}`);
                    resolve(JSON.parse(body_));
                }
            });
        }
    });
}

function merchVerify(req, res, next) {
    var access_token = req.headers['x-access-token'];
    if (!access_token) return res.status(403).send({ success: false, message: 'No access token provided.' });
    next();
}

module.exports = app;