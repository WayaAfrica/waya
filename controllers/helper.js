var db = require('./db');
var step_out = require("request");
var Multer = require('multer');
const {format} = require('util');
const {Storage} = require('@google-cloud/storage');
const storage = new Storage();
// const bucket = storage.bucket(process.env.GCLOUD_STORAGE_BUCKET);
const bucket = storage.bucket('wayastore');

const smileIdentityCore = require("smile-identity-core");
const IDApi = smileIdentityCore.IDApi;

var userUpload = Multer({ storage : Multer.memoryStorage() });

function saveFile(fileBuffer, res, folder, file_name) {
    return new Promise((resolve, reject) => {
        const blob = bucket.file(`${folder}/${file_name}`);
        const blobStream = blob.createWriteStream({
            resumable: false,
        });
        blobStream.on('error', err => {
            reject(err);
        });
        blobStream.on('finish', () => {
            const publicUrl = format(
                `https://storage.googleapis.com/${bucket.name}/${blob.name}`
            );
            resolve(publicUrl);
        });
        blobStream.end(fileBuffer);
    });
    
    
}

function makePostCall(data_, endpoint, auth_token = '', expected_json = true) {
    let options = getHeaders(data_, endpoint, auth_token);
    return new Promise(function(resolve, reject) {
        step_out.post(options, (err, res, body) => {
            if (res.statusCode == 403 || res.statusCode == 401) {
                resolve({ statusCode : res.statusCode, statusMessage : `An error occurred. Most likely, access is restricted`, errors : [ body ] });
            } else if (res.statusCode == 500) {
                resolve({ statusCode : '500', statusMessage : `We encountered a 500`, errors : [ body ] });
            }  else if (res.statusCode == 404) {
                resolve({ statusCode : '404', statusMessage : `We encountered a 404`, errors : [ 'Not Found' ] });
            } else {
                if (err) {
                    // console.log('PostErr', err);
                    reject(err);
                } else {
                    // console.log(body)
                    if (expected_json) {
                        resolve(JSON.parse(body));
                    } else {
                        resolve(body);
                    }
                }
            }
        });
    });
}
function makePutCall(data_, endpoint, auth_token = '', expected_json = true) {
    let options = getHeaders(data_, endpoint, auth_token);
    return new Promise(function(resolve, reject) {
        step_out.put(options, (err, res, body) => {
            if (res.statusCode == 403 || res.statusCode == 401) {
                resolve({ statusCode : '000', statusMessage : `An error occurred. Most likely, access is restricted`, errors : [ res.statusMessage ] });
            } else if (res.statusCode == 500) {
                resolve({ statusCode : '500', statusMessage : `We encountered a 500`, errors : [ res.statusMessage ] });
            } else if (res.statusCode == 404) {
                resolve({ statusCode : '404', statusMessage : `We encountered a 404`, errors : [ 'Not Found' ] });
            } else {
                if (err) {
                    reject(err);
                } else {
                    if (expected_json) {
                        resolve(JSON.parse(body));
                    } else {
                        resolve(body);
                    }
                }
            }
        });
    });
}
function makeGetCall(data_, endpoint, auth_token = '', expected_json = true) {
    let options = getHeaders(data_, endpoint, auth_token);
    return new Promise(function(resolve, reject) {
        step_out.get(options, (err, res, body) => {
            if (res.statusCode == 403 || res.statusCode == 401) {
                resolve({ statusCode : res.statusCode, statusMessage : `An error occurred. Most likely, access is restricted`, errors : [ res.statusMessage ] });
            } else if (res.statusCode == 500) {
                resolve({ statusCode : '500', statusMessage : `We encountered a 500`, errors : [ body ] });
            } else if (res.statusCode == 404) {
                resolve({ statusCode : '404', statusMessage : `We encountered a 404`, errors : [ 'Not Found' ] });
            } else {
                if (err) {
                    console.log(err);
                    reject(err);
                } else {
                    // console.log(body);
                    if (expected_json) {
                        resolve(JSON.parse(body));
                    } else {
                        resolve(body);
                    }
                }
            }
        });
    });
}
function makeDelCall(data_, endpoint, auth_token = '', expected_json = true) {
    let options = getHeaders(data_, endpoint, auth_token);
    return new Promise(function(resolve, reject) {
        step_out.delete(options, (err, res, body) => {
            if (res.statusCode == 500 || res.statusCode == 403 || res.statusCode == 401) {
                resolve({ statusCode : '000', statusMessage : `An error occurred. Most likely, access is restricted`, errors : [ 'Unauthorised Access' ] });
            } else {
                if (err) {
                    reject(err);
                } else {
                    if (expected_json) {
                        resolve(JSON.parse(body));
                    } else {
                        resolve(body);
                    }
                }
            }
        });
    });
}

function talkBack(res, code, word, json_ = false) {
    if (!json_) {
        res.writeHead(code, {'Content-type' : 'text/plain'});
        res.end(word);
        return;
    }
    res.json(word);
}
function outPut(rez, source, token = '') {
    if (token == '') {
        if (source == '') {
            talkBack(rez, 200, { success:true, data:source }, true);
        } else if (!("statusCode" in source) && !("status" in source)) {
            talkBack(rez, 200, { success:true, data:source }, true);
        } else {
            if ("statusCode" in source) {
                if (source.statusCode == "000.001") {
                    talkBack(rez, 200, { success:true, message:source.statusMessage, data:source.data }, true);
                } else {
                    talkBack(rez, source.statusCode, { success:false, response: source.statusMessage, error: source.errors }, true);
                } 
            } else if ("status" in source) {
                talkBack(rez, source.statusCode, { success:false, response: `An error occurred. ${source.message}. Token may have expired` }, true);
            }
        }
    } else {
        if ("status" in source) {
            console.log(source.status);
            if (source.message == 'Access Denied') {
                talkBack(rez, 401, { success:false, response: 'Session expired' }, true);
            }
        }
        talkBack(rez, 200, source, true);
    }
}

function getHeaders(_data, endpoint, auth) {
    if (auth != '') {
        // console.log(`Auth provided`);
        return {
            headers: { 'Content-Type':'application/json', 'Authorization': `Bearer ${auth}` },
            url: process.env.BASE + endpoint,
            body: _data,
        };
    } else {
        // console.log("No Auth Required");
        return {
            headers: { 'Content-Type':'application/json' },
            url: process.env.BASE + endpoint,
            body: _data,
        };
    }
}

// function sendSMS(msisdn, message, sender = 'WayaMoney') {
//     var opt = {
//         headers: { "Content-Type" : "application/json" },
//         url: `https://api.mnotify.com/api/sms/quick?key=${process.env.NOTIFY}`,
//         body: JSON.stringify({"recipient":[msisdn], "sender":sender, message: message})
//     };
//     return new Promise(function(resolve, reject) {
//         step_out.post(opt, (err, res, body) => {
//             if (err) {
//                 reject(err);
//             } else {
//                 var parsedBody = JSON.parse(body);
//                 resolve(parsedBody);
//             }
//         });
//     });
// }

function getHLR(msisdn) {
    var opt = {
        headers: { 'Authorization': `AccessKey ${process.env.BIRDKEY}` },
        url: 'https://rest.messagebird.com/hlr/',
        body: JSON.stringify({"msisdn":msisdn})
    };
    return new Promise(function(resolve, reject) {
        step_out.post(opt, (err, res, body) => {
            if (err) {
                reject(err);
            } else {
                var parsedBody = JSON.parse(body);
                resolve(parsedBody.href);
            }
        });
    });
}
function viewHLR(path) {
    var opt = {
        headers: { 'Authorization': `AccessKey ${process.env.BIRDKEY}` },
        url: path,
    };
 
    return new Promise(function(resolve, reject) {
        step_out.get(opt, (err_, res_, body_) => {
            if (err_) {
				reject(JSON.parse(err_));
			} else {
                resolve(JSON.parse(body_));
            }
        });
    });
}

function sendSMS(mobileNumber, content) {
    return new Promise((resolve, reject) => {
        var messagebird = require('messagebird')('a9jP47Isk19HhveGKHaKiinVz');
        var params = {
            'originator': 'WayaMoney',
            'recipients': [ mobileNumber ],
            'body': content
        };
        messagebird.messages.create(params, function (err, response) {
            if (err) {
                console.log(err);
                reject(false);
            }
            if (response.recipients.totalSentCount == 1) {
                resolve(true);
            } else {
                reject(false);
            }            
        });
    })
    
}

function genId(){
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let autoId = '';
    for (let i = 0; i < 20; i++) {
        autoId += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    // console.log('Returning id ', autoId);
    return autoId;
}

function mapID(id) {
    var smileType;
    switch (id) {
        case 1:
        case 7:
        case 13:
            smileType = 'PASSPORT';
            break;
        case 9:
        case 3:
            smileType = 'VOTER_ID'
            break;
        case 8:
        case 2:
            smileType = 'DRIVERS_LICENSE';
            break;
        case 4:
            smileType = 'SSNIT';
            break;
        case 5:
            smileType = 'BVN';
            break;
        case 6:
            smileType = 'NIN';
            break;
        case 11:
            smileType = 'TIN';
            break;
        case 12:
            smileType = 'CAC';
            break;
        case 14:
            smileType = 'NATIONAL_ID';
            break;
        case 15:
            smileType = 'ALIEN_CARD';
            break;
}
    return smileType;
}

function smile(user_, id_info) {
    const connection = new IDApi(process.env.SMILEID, process.env.SMILEKEY, 0);

    // Create required tracking parameters
    let partner_params = {
        user_id: user_,
        job_id: genId(),
        job_type: 5
    };

    console.log('PartnerID', process.env.SMILEID);
    console.log('APIKEY', process.env.SMILEKEY);
    console.log('PartnerParams', partner_params);
    console.log('IdInfo', id_info);

    return new Promise((g, b) => {
        response = connection.submit_job(partner_params, id_info).then(response => {
            console.log(response);
            g(response)
        }, err => {
            b(err);
        });
    });
}

function awaitIDResponse(pendingId) {
    return new Promise((good, bad) => {
        var que = `insert into idVerifications (pendingId) values (?)`;
        db.execute(que, [ pendingId ]).then((rx) => {
            if (rx.success) {
                good(true);
            } else {
                bad(false);
            }
        }, (problem) => {
            console.log(problem);
            bad(problem);
        });
    })
    
}

function finalizeIDResponse(pendingId, coreId, smileJobId, resultText, resultCode) {
    var que = `update idVerifications set coreId = ?, SmileJobID = ?, ResultText = ?, ResultCode = ? where pendingId = ?`;
    db.update(que, [ coreId, smileJobId, resultText, resultCode, pendingId ]).then((rx) => {
        return rx.success;
    }, (problem) => {
        waya.out(res, problem, true);
    });
}

module.exports = {
    'out' : outPut,
    'talk' : talkBack,
    'getCall' : makeGetCall,
    'postCall' : makePostCall,
    'putCall' : makePutCall,
    'delCall' : makeDelCall,
    'HLR' : getHLR,
    'SMS' : sendSMS,
    'viewHLR' : viewHLR,
    'userupload' : userUpload,
    'saveImg' : saveFile,
    smile,
    mapID,
    awaitIDResponse,
    finalizeIDResponse,
    genId,
    sendSMS,
}