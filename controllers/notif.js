var waya = require('../controllers/helper');
var vfy = require('../authentication/verify');
var express = require('express');
var path = require('path');
var nodemailer = require('nodemailer');
var route = express();
route.set("view engine", "pug");
route.set("views", path.join(__dirname, "../mails"));
var bod = require('body-parser');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());
const pug = require('pug');
var admin = require("firebase-admin");
var serviceAccount = require("../wayafire.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://waya-mobile.firebaseio.com"
});

route.post('/all', vfy, function(req, res) {
    
});

route.post('/sms', vfy, function(req, res) {
    waya.postCall(JSON.stringify(req.body), 'notify/sms', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.post('/app', vfy, function(req, res) {
    pushNotification(res, req.body.message, req.body.title, req.body.token);
});

route.get('/rawhtml', function(req, res) {
    res.sendFile(path.join(__dirname, '../mails', 'raw.html'));
});
route.get('/rawpug', function(req, res) {
    res.render("raw", {
        name: 'Bruno'
    });
});

route.post('/mail', function(req, res) {
    waya.postCall(JSON.stringify(req.body), 'notify/email', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
    
    // showMail(req, res).catch(err => {
    //     console.error(err.message);
    //     process.exit(1);
    // });
});

function pushNotification(res, message_, title_, token_) {
    if (title_ == '' || !title_) title_ = 'WayaMoney Hub';
    var payload = { 'data' : { 'title' : title_, 'message': message_ } };
    var fcmopt = { 'priority' : 'high' };

    if (!token_ && token_ != '') {
        admin.messaging().sendToTopic("general", payload, fcmopt)
        .then(great => {
            console.log("Went well: " + JSON.stringify(great))
            waya.talk(res, 200, {success : true, messageId : great.messageId }, true);
        }, (wrong) => {
            console.log("Went bad: " + JSON.stringify(wrong))
            waya.talk(res, 500, {success : false, message : wrong }, true);
        });
    } else {
        payload.token = token_;
        console.log(payload);
        admin.messaging().send(payload)
        .then((great) => {
            console.log('Successfully sent message:', great);
            waya.talk(res, 200, {success : true, messageId : great }, true);
        })
        .catch((wrong) => {
            console.log('Error sending message:', wrong);
            waya.talk(res, 500, {success : false, message : wrong }, true);
        });
    }
}

async function showMail(req, res) {
    recipient = req.body.recipient;
    subject = req.body.title;
    fname = req.body.name;
    data = pug.renderFile(path.join(__dirname, `../mails/${req.body.class}.pug`), {
        name: fname,
        recipient: recipient,
        title: subject,
        role: req.body.options.role,
        token: req.body.options.reset_token,
        merchant: req.body.options.merchant,
    });
    // data = res.render(req.body.class, {
    //     name: fname,
    //     recipient: recipient,
    //     title: subject,
    //     role: req.body.options.role,
    //     merchant: req.body.options.merchant,
    // });

    let transporter = nodemailer.createTransport({
        host: 'mailer1.expressbox.email',
        port: 465,
        secure: true,
        auth: {
            user: 'noreply@wayamoney.com',
            pass: process.env.WAYAMLPASS
        }
    });
    
    console.log('Attempting...');
    let info = await transporter.sendMail({
        from: '"Waya Money" <noreply@wayamoney.com>',
        to: recipient,
        subject: subject,
        html: data
    });
    console.log('Message sent: %s', info.messageId);
    waya.talk(res, 200, {success : true, message : `Mail sent successfully to ${recipient}`}, true);
}

module.exports = route;