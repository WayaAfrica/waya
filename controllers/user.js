var db = require('./db');
var waya = require('../controllers/helper');
var vfy = require('../authentication/verify');
var express = require('express');
var route = express();
var bod = require('body-parser');
const uuid = require('uuid/v4');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());

route.get('/all', vfy, function(req, res) {
    waya.getCall('', 'users/all', req.headers['x-access-token']).then((resp) => {
        waya.out(res, resp, '');
    });
});

route.get('/me', vfy, function(req, res) {
    waya.getCall('', 'users/me', req.headers['x-access-token']).then((resp) => {
        let folder = 'merchants';
        if ("roles" in resp) {
            if (resp.roles[0] == 'ROLE_USER') folder = 'users';
            resp.displayImage = `https://storage.googleapis.com/wayastore/${folder}/${resp.userIdentifier.userID}.jpg`;
        }
        
        //Send Response to Client
        waya.out(res, resp, '');
    }, (prob) => {
        console.log(`Problem: ${prob}`);
    });
});
  
route.post('/setDP/:id', waya.userupload.single('avi'), (req, res) => {
    if (!req.file) {
        res.status(400).send('No file uploaded.');
        waya.talk(res, 400, { success : false, message : 'No file uploaded' }, true);
        return;
    }
    waya.saveImg(req.file.buffer, res, 'users', `${req.params.id}.jpg`).then(publicUrl => {
        waya.talk(res, 200, { success : true, message : 'Photo uploaded successfully', uploadedTo : publicUrl }, true);
    }, err => {
        waya.talk(res, 400, { success : false, message : err }, true);
    });
});

route.put('/changePassword', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), 'users/password', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.post('/create', waya.userupload.fields([{name:'users', maxCount:1}, {name:'photo_id', maxCount:1}]), (req, res) => {
    req.body.selfieUrl = waya.genId();
    req.body.idPhotoUrl = waya.genId();
    req.body.idNumber = waya.genId();
    req.body.nameOnId = `${req.body.firstName} ${req.body.lastName}`;
    req.body.idIssueDate = '2020-05-05';
    req.body.idType = {id: 1};
    delete req.body.phoneNumberDef;

    //Process signup
    waya.postCall(JSON.stringify(req.body), 'users/signup', '').then((resp) => {
        
        // Append Role
        if (resp.data != null ) {
            resp.data.role = 'Regular';
        }

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.post('/createTest2', waya.userupload.fields([{name:'reg_cert', maxCount:1}, {name:'photo_id', maxCount:1}]), (req, res) => {
    req.body.fish = 'Salmon';
    var fileKeys = Object.keys(req.files);
    fileKeys.forEach(function(key) {
        waya.saveImg(req.files[key][0].buffer, res, key, `${uuid()}.jpg`).then(path => {
            console.log('KEY: '+key);
            console.log('PATH: '+path);
            if (key == 'reg_cert') {
                req.body.registrationCertificateUrl = path;
            } else if (key == 'photo_id') {
                req.body.idPhotoUrl = path;
                // waya.talk(res, 200, { success : true, message : req.body }, true);

                //Process signup
                waya.postCall(JSON.stringify(req.body), 'users/signup', '').then((resp) => {

                    // Append Role
                    if (resp.data != null ) {
                        resp.data.role = 'Regular';
                    }

                    //Send Response to Client
                    waya.out(res, resp, '');
                });
            }
        }, err => {
            console.log('ERR: ' + err);
        });
    });

    if (fileKeys.length == 0) {
        waya.talk(res, 400, { success : false, message : 'No file uploaded' }, true);
    }
});

route.get('/:username', vfy, (req, res) => {
    waya.getCall('', `users/findUser/${req.params.username}`, req.headers['x-access-token']).then((resp) => {

        //Check error
        if (resp.status == 500 || resp.status == 403) {
            waya.talk(res, 401, { success:false, response: `An error occurred. ${resp.message}`}, true);
            return false;
        }

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.put('/:username', vfy, (req, res) => {
    waya.putCall(JSON.stringify(req.body), `users/update/${req.params.username}`, req.headers['x-access-token']).then((data) => {
        waya.talk(res, 200, data, true);
    });
});

route.post('/createFavorite', (req, res) => {
    let body_ = req.body;
    let userId = body_.userId;
    let fav = body_.favoriteAccount
    let countryCode = fav.countryCode;
    let accountType = fav.accountType;
    let favName = '';
    let accountNumber = '';
    let issuer = '';
    let bankName = '';
    let bankBranch = '';
    let bankAccountType = '';
    if (fav.mobileMoneyAccount != undefined) {
        let  momo = fav.mobileMoneyAccount;
        favName = momo.mobileMoneyName;
        accountNumber = momo.accountNumber;
        issuer = momo.accountIssuer;
    } else if (fav.bankAccount != undefined) {
        let bank = fav.bankAccount;
        favName = bank.accountName;
        accountNumber = bank.accountNumber;
        bankName = bank.bankName;
        bankBranch = bank.bankBranch;
        bankAccountType = bank.accountType;
    }
    var que = `insert into favorites (userId, favName, accountNumber, bankName, branch, bankAccountType, issuer, accountType, countryCode) values (?)`;
    db.execute(que, [ userId, favName, accountNumber, bankName, bankBranch, bankAccountType, issuer, accountType, countryCode ]).then((rx) => {
        if (rx.success) {
            waya.out(res, { message: 'Favorite Saved Successfully' }, false);
        } else {
            waya.out(res, { message : rx.message }, true);
        }
    }, (problem) => {
        waya.out(res, problem, true);
    });
});

route.post('/updateFavorite/:id', (req, res) => {
    let id = req.params.id;
    let body_ = req.body;
    let userId = body_.userId;
    let fav = body_.favoriteAccount
    let countryCode = fav.countryCode;
    let accountType = fav.accountType;
    let favName = '';
    let accountNumber = '';
    let issuer = '';
    let bankName = '';
    let bankBranch = '';
    let bankAccountType = '';
    if (fav.mobileMoneyAccount != undefined) {
        let momo = fav.mobileMoneyAccount;
        favName = momo.mobileMoneyName;
        accountNumber = momo.accountNumber;
        issuer = momo.accountIssuer;
    } else if (fav.bankAccount != undefined) {
        let bank = fav.bankAccount;
        favName = bank.accountName;
        accountNumber = bank.accountNumber;
        bankName = bank.bankName;
        bankBranch = bank.bankBranch;
        bankAccountType = bank.accountType;
    }
    var que = "update favorites set favName = ?, accountNumber = ?, bankName = ?, branch = ?, bankAccountType = ?, issuer = ?, accountType = ?, countryCode = ? where userId = ? and id = ?";
    db.update(que, [ favName, accountNumber, bankName, bankBranch, bankAccountType, issuer, accountType, countryCode, userId, id ]).then((rx) => {
        if (rx.success) {
            waya.out(res, { message: 'Favorite Updated Successfully' }, false);
        } else {
            waya.out(res, { message : rx.message }, true);
        }
    }, (problem) => {
        waya.out(res, problem, true);
    });
});

route.get('/getFavorites/:userId/', (req, res) => {
    let que = `select * from favorites where userId = '${req.params['userId']}'`;
    db.fetch(que).then((data) => {
        waya.out(res, data, true);
    }, (problem)=>{
        waya.out(res, problem, true);
    });
});

route.delete('/deleteFavorite/:id/', (req, res) => {
    let que = `delete from favorites where id = ?`;
    db.execute(que, [ req.params['id'] ]).then((data) => {
        if (data.success)
            waya.out(res, { message: 'Favorite Deleted Successfully' }, true);
        else
            waya.talk(res, data.message, true);
    }, (problem)=>{
        waya.out(res, problem, true);
    });
});

route.post('/smile', (req, res) => {
    waya.smile('123456789').then(stuff => {
        res.json(stuff);
    }, error => {
        console.log('ERR:', error);
    })
})

module.exports = route;