var waya = require('../controllers/helper');
var vfy = require('../authentication/verify');
var express = require('express');
var route = express();
var bod = require('body-parser');
const mcc_mnc_list = require('mcc-mnc-list');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());

route.get('/', function(req, res) {
    query = '';
    if (req.url.indexOf('?') != -1)
        query = req.url.substr(req.url.indexOf('?') + 1);
    waya.getCall('', `country/getCountries?${query}`, req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.get('/hlr/:msisdn/:country', vfy, (req, res) => {
    waya.HLR(req.params.msisdn).then((hlr_href) => {
        
        setTimeout(() => {
            waya.viewHLR(hlr_href).then((success) => {
                var network = success.network;

                if (network != undefined && network != '' && network != null) {
                    filtered = mcc_mnc_list.filter({ mccmnc : network });
                    waya.talk(res, 200, filtered[0].brand, true);
                    return false;
                }

                telcoArr = [];
                countryTelcos = mcc_mnc_list.filter({ countryName : req.params.country });
                for (var i = 0; i < countryTelcos.length; i++) {
                    brand = countryTelcos[i].brand;
                    if (brand != undefined && brand != '' && brand != null) {
                        telcoArr.push(brand);
                    }
                }
                waya.talk(res, 200, telcoArr, true);

            }, (failure) => {
                waya.talk(res, 401, failure);
            });
        }, 4000);

    }, (main_failure) => {
        waya.talk(res, 401, main_failure);
    });
})

module.exports = route;