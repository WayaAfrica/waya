var waya = require('./helper');
var vfy = require('../authentication/verify');
var express = require('express');
var router = express();
var bod = require('body-parser');
router.use(bod.urlencoded({ extended: true }));
router.use(bod.json())

router.post('/signin', (req, res) => {
  //Check auth
  var api_key = req.headers['x-api-key'];
  if (!api_key) return res.status(403).send({ success: false, message: 'No api key provided.' });
  if (api_key !== 'ergrgerg8273y29738971803ejh12909023u10eu3u2e93') return res.status(500).send({ success: false, message: 'Failed to authenticate api_key.' });
  
  waya.postCall(JSON.stringify(req.body), 'auth/signin', '', true).then((resp) => {

    console.log(resp);

    if (resp.status == 401 || !resp.token) {
      let rx = resp.errors[0];
      let errorMessage = JSON.parse(rx).message;
      waya.talk(res, 401, { success:false, response: errorMessage}, true);
      return false;
    }

    //Send Response to Client
    waya.talk(res, 200, { success:true, response:resp }, true);
  });
});

router.post('/token/refresh', vfy, (req, res) => {
  
  //Get response from DB
  waya.postCall(JSON.stringify(req.body), 'auth/token/refresh', req.headers['x-access-token'], true).then((resp) => {

    if (resp.status == 500 || resp.status == 403 || !resp.token) {
      waya.talk(res, 401, { success:false, response: `An error occurred. ${resp.message}`}, true);
      return false;
    }

    // Save Token
    var token = resp.token;

    //Send Response to Client
    waya.out(res, { success:true, response:resp }, token);
  });
});


// Password Forgotten
router.post('/forgotPassword', (req, res) => {
  waya.postCall(JSON.stringify(req.body), 'password/forget', '', true).then((resp) => {
    waya.out(res, resp, '');
  });
});

router.post('/resetPassword', (req, res) => {
  waya.postCall(JSON.stringify(req.body), 'password/forget/change', '', true).then((resp) => {
    waya.out(res, resp, '');
  });
});

router.post('/validateForgotPassword', (req, res) => {
  waya.postCall(JSON.stringify(req.body), 'password/forget/validate', '', true).then((resp) => {
    waya.out(res, resp, '');
  });
});

module.exports = router;