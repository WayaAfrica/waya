var db = require('./db');
var waya = require('../controllers/helper');
var vfy = require('../authentication/verify');
var express = require('express');
var route = express();
var bod = require('body-parser');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());

//Transfers
route.post('/quote', vfy, function(req, res) {
    waya.postCall(JSON.stringify(req.body), `transaction/quote`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.post('/transfer', vfy, (req, res) => {
    if (req.body.config.description.includes("Request Payment")) {
        return res.json({success: false, response: "Sorry, this feature is currently suspended, please try again later, please contact Wayamoney for authorization"});
    }
    //Check if destination account is momo
    if (req.body.destination.accountType === 'MobileWallet') {
        let recipientName = req.body.destination.mobileMoneyAccount.mobileMoneyName;
        let recipientMobile = req.body.destination.mobileMoneyAccount.accountNumber;
        let currency = req.body.recipientAmount.recipientCurrency
        let amount = req.body.recipientAmount.recipientAmount
        let reference = waya.genId();
            
        //Check sender account type
        if (req.body.source.accountType === 'MobileWallet') {
            let senderName = req.body.source.mobileMoneyAccount.mobileMoneyName;
            let senderMobile = req.body.source.mobileMoneyAccount.accountNumber;
            var que = `insert into transactions (reference, senderName, senderMobile, recipientName, recipientMobile, recipientCurrency, recipientAmount) values (?)`;
            db.execute(que, [ reference, senderName, senderMobile, recipientName, recipientMobile, currency, amount ]).then((rx) => {
                if (rx.success) {
                    waya.postCall(JSON.stringify(req.body), `transaction/transfer`, req.headers['x-access-token']).then((resp) => {
                        let transactionId = resp.id;
                        db.update( 'update transactions set transactionId = ? where reference = ?', [ transactionId, reference ] );
                        waya.out(res, resp, '');
                    });
                } else {
                    waya.out(res, { message : rx.message }, true);
                }
            });

        } else if (req.body.source.accountType === 'Card') {
            let senderName = req.body.source.cardAccount.cardAccountName;
            var que = `insert into transactions (reference, senderName, recipientName, recipientMobile, recipientCurrency, recipientAmount) values (?)`;
            db.execute(que, [ reference, senderName, recipientName, recipientMobile, currency, amount ]).then((rx) => {
                if (rx.success) {
                    waya.postCall(JSON.stringify(req.body), `transaction/transfer`, req.headers['x-access-token']).then((resp) => {
                        let transactionId = resp.id;
                        db.update( 'update transactions set transactionId = ? where reference = ?', [ transactionId, reference ] );
                        waya.out(res, resp, '');
                    });
                } else {
                    waya.out(res, { message : rx.message }, true);
                }
            });
        }
    } else {
        //No momo involved
        waya.postCall(JSON.stringify(req.body), `transaction/transfer`, req.headers['x-access-token']).then((resp) => {            
            waya.out(res, resp, '');
        });
    }
});
route.post('/m2m', vfy, function(req, res) {
    if (req.body.config.description.includes("Request Payment")) {
        return res.json({success: false, response: "Sorry, this feature is currently suspended, please try again later, please contact Wayamoney for authorization"});
    }
    if (req.body.source.accountType !== 'MobileWallet' || req.body.destination.accountType !== 'MobileWallet') {
        waya.talk(res, 401, { success:false, response: `Source or destination account types not accurate`}, true);
        return false;
    }

    let senderName = req.body.source.mobileMoneyAccount.mobileMoneyName;
    let senderMobile = req.body.source.mobileMoneyAccount.accountNumber;
    let recipientName = req.body.destination.mobileMoneyAccount.mobileMoneyName;
    let recipientMobile = req.body.destination.mobileMoneyAccount.accountNumber;
    let currency = req.body.recipientAmount.recipientCurrency
    let amount = req.body.recipientAmount.recipientAmount
    let reference = waya.genId();

    var que = `insert into transactions (reference, senderName, senderMobile, recipientName, recipientMobile, recipientCurrency, recipientAmount) values (?)`;
    db.execute(que, [ reference, senderName, senderMobile, recipientName, recipientMobile, currency, amount ]).then((rx) => {
        if (rx.success) {
            waya.postCall(JSON.stringify(req.body), `transaction/transfer`, req.headers['x-access-token']).then((resp) => {
                let transactionId = resp.id;
                db.update( 'update transactions set transactionId = ? where reference = ?', [ transactionId, reference ] );

                //Send Response to Client
                waya.out(res, resp, '');
            });
        } else {
            waya.out(res, { message : rx.message }, true);
        }
    });
    
});
route.post('/c2m', vfy, function(req, res) {
    if (req.body.config.description.includes("Request Payment")) {
        return res.json({success: false, response: "Sorry, this feature is currently suspended, please try again later, please contact Wayamoney for authorization"});
    }
    if (req.body.source.accountType !== 'Card' || req.body.destination.accountType !== 'MobileWallet') {
        waya.talk(res, 401, { success:false, response: `Source or destination account types not accurate`}, true);
        return false;
    }

    let senderName = req.body.source.cardAccount.cardAccountName;
    let recipientName = req.body.destination.mobileMoneyAccount.mobileMoneyName;
    let recipientMobile = req.body.destination.mobileMoneyAccount.accountNumber;
    let currency = req.body.recipientAmount.recipientCurrency
    let amount = req.body.recipientAmount.recipientAmount
    let reference = waya.genId();

    var que = `insert into transactions (reference, senderName, recipientName, recipientMobile, recipientCurrency, recipientAmount) values (?)`;
    db.execute(que, [ reference, senderName, recipientName, recipientMobile, currency, amount ]).then((rx) => {
        if (rx.success) {
            waya.postCall(JSON.stringify(req.body), `transaction/transfer`, req.headers['x-access-token']).then((resp) => {

                let transactionId = resp.id;
                db.update( 'update transactions set transactionId = ? where reference = ?', [ transactionId, reference ] );
                
                //Send Response to Client
                waya.out(res, resp, '');
            });
        } else {
            waya.out(res, { message : rx.message }, true);
        }
    });
});

route.post('/card/checkout/validate', vfy, function(req, res) {
    waya.postCall(JSON.stringify(req.body), `transaction/card/checkout/validate`, req.headers['x-access-token']).then((resp) => {
        //Send Response to Client
        waya.out(res, resp, '');
    });
});


//Payments
route.post('/payment', vfy, function(req, res) {
    data = req.body;
    waya.postCall(JSON.stringify(data), `transaction/payment`, req.headers['x-access-token']).then((resp) => {
        waya.out(res, resp, '');
    }, (err)=>{
        console.log('Errorz:'+ err);
    });
});
route.get('/listPayments', vfy, function(req, res) {
    waya.getCall('', `transaction/payment/list`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/searchPayments', vfy, function(req, res) {
    query = '';
    if (req.url.indexOf('?') != -1)
        query = req.url.substr(req.url.indexOf('?') + 1);
    waya.getCall('', `transaction/payment/search?${query}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getPaymentStatus/:paymentStatusID', vfy, (req, res) => {
    waya.getCall('', `transaction/payment/${req.params.paymentStatusID}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

//Tellernet
route.post('/tellernet/card-transaction/response', vfy, (req, res) => {
    data = req.body;
    waya.postCall(JSON.stringify(data), 'tellernet/card-transaction/response', req.headers['x-access-token'], false).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});


//Settlements
route.get('/searchSettlements', vfy, function(req, res) {
    query = '';
    if (req.url.indexOf('?') != -1)
        query = req.url.substr(req.url.indexOf('?') + 1);
    waya.getCall('', `transaction/settlement/search?${query}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});


//Refunds
route.post('/requestRefund/:paymentStatusID', vfy, function(req, res) {
    waya.postCall(JSON.stringify(req.body), `transaction/payment/${req.params.paymentStatusID}/refund`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/listRefunds', vfy, function(req, res) {
    waya.getCall('', 'transaction/payment/refund/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getRefundStatus/:paymentStatusID/:refundUUID', vfy, (req, res) => {
    waya.getCall('', `transaction/payment/${req.params.paymentStatusID}/refund/${req.params.refundUUID}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.post('/approveRefund/:paymentStatusID/:refundUUID', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), `transaction/payment/${req.params.paymentStatusID}/refund/${req.params.refundUUID}/approve`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.post('/issueRefund/:paymentStatusID/:refundUUID', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), `transaction/payment/${req.params.paymentStatusID}/refund/${req.params.refundUUID}/issue`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.post('/denyRefund/:paymentStatusID/:refundUUID', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), `transaction/payment/${req.params.paymentStatusID}/refund/${req.params.refundUUID}/deny`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.post('/rejectRefund/:paymentStatusID/:refundUUID', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), `transaction/payment/${req.params.paymentStatusID}/refund/${req.params.refundUUID}/reject`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});


//Payouts
route.post('/payout', vfy, function(req, res) {
    waya.postCall(JSON.stringify(req.body), `transaction/payout`, req.headers['x-access-token']).then((resp) => {

        //Check error
        if (resp.status == 500 || resp.status == 403) {
            waya.talk(res, 401, { success:false, response: `An error occurred. ${resp.message}`}, true);
            return false;
        }

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/searchPayouts', vfy, function(req, res) {
    query = '';
    if (req.url.indexOf('?') != -1)
        query = req.url.substr(req.url.indexOf('?') + 1);
    waya.getCall('', `transaction/payout/search?${query}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.get('/getStatus/:trx_id', vfy, function(req, res) {
    waya.getCall('', `transaction/${req.params.trx_id}`, req.headers['x-access-token']).then((resp) => {
        
        //Send SMS only if transaction was successfull
        if (resp.overallStatus === 'SUCCESS') {
            let transactionId = req.params.trx_id;
            let que = `select * from transactions where transactionId = '${transactionId}' and recipientNotified = 0`;
            db.fetch(que).then((response) => {
                if (response.data.length == 1) {
                    let senderName = response.data[0].senderName;
                    let recipientName = response.data[0].recipientName;
                    let mobile = response.data[0].recipientMobile;
                    let currency = response.data[0].recipientCurrency;
                    let amount = response.data[0].recipientAmount;
                    let message = `Hello ${recipientName}, ${senderName} sent you ${currency+amount} via the WayaMoney app.`;
                    waya.sendSMS(mobile, message).then(done => {
                        if (done)
                            db.update( 'update transactions set recipientNotified = 1, transactionStatus = 1 where transactionId = ?', [ transactionId ] ).then(x => {
                                if (x.success)
                                    console.log('done');
                                else
                                    console.log('not done');
                            });
                    });
                } else {
                    console.log('maybe confirmed');
                }
            }, (problem) => {
                waya.out(res, problem, true);
                return;
            });
        }

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.get('/search', vfy, function(req, res) {
    query = '';
    if (req.url.indexOf('?') != -1)
        query = req.url.substr(req.url.indexOf('?') + 1);
    waya.getCall('', `transaction/search?${query}`, req.headers['x-access-token']).then((resp) => {

        //check for errors, check status and send sms
        if (!("statusCode" in resp)) {
        
            //Send Response to Client
            waya.out(res, resp, '');
            
            //Send sms if necessary
            for (let i = 0; i < resp.content.length; i++) {
                let transaction = resp.content[i];
                if (transaction.overallStatus === 'SUCCESS') {
                    let transactionId = req.params.trx_id;
                    let que = `select * from transactions where transactionId = '${transactionId}' and recipientNotified = 0`;
                    db.fetch(que).then((response) => {
                        if (response.data.length == 1) {
                            let senderName = response.data[0].senderName;
                            let recipientName = response.data[0].recipientName;
                            let mobile = response.data[0].recipientMobile;
                            let currency = response.data[0].recipientCurrency;
                            let amount = response.data[0].recipientAmount;
                            let message = `Hello ${recipientName}, ${senderName} sent you ${currency+amount} via the WayaMoney app.`;
                            waya.sendSMS(mobile, message).then(done => {
                                if (done)
                                    db.update( 'update transactions set recipientNotified = 1, transactionStatus = 1 where transactionId = ?', [ transactionId ] ).then(x => {
                                        if (x.success)
                                            console.log('done');
                                        else
                                            console.log('not done');
                                    });
                            });
                        } else {
                            console.log('maybe confirmed');
                        }
                    }, (problem) => {
                        console.log('db connection issue');
                    });
                } else {
                    console.log(transaction.overallStatus);
                }
            }
        }
    });
});

module.exports = route;