var waya = require('../controllers/helper');
var express = require('express');
var route = express();
var bod = require('body-parser');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());

route.get('/idtypeList', function(req, res) {
    waya.getCall('', 'idtype/list', req.headers['x-access-token']).then((resp) => {
        waya.out(res, resp, '');
    });
});

route.get('/idtypeList/:countryCode', function(req, res) {
    waya.getCall('', `idtype/list/${req.params.countryCode}`, req.headers['x-access-token']).then((resp) => {
        waya.out(res, resp, '');
    });
});

module.exports = route;