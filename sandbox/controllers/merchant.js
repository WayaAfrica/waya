var db = require('./db');
var waya = require('../controllers/helper');
var vfy = require('../authentication/verify');
var express = require('express');
var route = express();
var bod = require('body-parser');
const uuid = require('uuid/v4');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());
route.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

route.post('/setDP/:id', waya.userupload.single('avi'), (req, res, next) => {
    if (!req.file) {
        waya.talk(res, 400, { success : false, message : 'No file uploaded' }, true);
        return;
    }
    waya.saveImg(req.file.buffer, res, 'merchants', `${req.params.id}.jpg`).then(publicUrl => {
        waya.talk(res, 200, { success : true, message : 'Photo uploaded successfully', uploadedTo : publicUrl }, true);
    }, err => {
        waya.talk(res, 400, { success : false, message : err }, true);
    });
});

route.post('/create', waya.userupload.fields([{name:'reg_cert', maxCount:1}, {name:'photo_id', maxCount:1}]), (req, res) => {
    req.body.registrationCertificateUrl = waya.genId();
    req.body.idPhotoUrl = waya.genId();
    req.body.idNumber = waya.genId();
    req.body.nameOnId = waya.genId();
    req.body.idType = {id: 1};
    req.body.idIssueDate = '2020-05-05';
    delete req.body.phoneNumber;
    delete req.body.phoneNumberDef;

    //Process signup
    waya.postCall(JSON.stringify(req.body), 'merchant/signup', '').then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.post('/createTest', waya.userupload.fields([{name:'reg_cert', maxCount:1}, {name:'photo_id', maxCount:1}]), (req, res) => {
    var fileKeys = Object.keys(req.files);
    reg_cert_set = true;
    photo_id_set = true;
    // fileKeys.forEach(function(key) {
    //     waya.saveImg(req.files[key][0].buffer, res, key, `${uuid()}.jpg`).then(path => {
    //         if (key == 'reg_cert') {
    //             req.body.registrationCertificateUrl = path;
    //             reg_cert_set = true;
    //         } else if (key == 'photo_id') {
    //             req.body.idPhotoUrl = path;
    //             photo_id_set = true;
    //         }
            
            if (reg_cert_set && photo_id_set) {
                req.body.idType = {id: parseInt(req.body.idType)};

                let id_info = {
                    first_name: 'Kwame',
                    last_name: 'Twum',
                    country: req.body.countryCode,
                    id_type: waya.mapID(req.body.idType.id),
                    id_number: req.body.idNumber,
                    phone_number: req.body.businessPhoneNumber,
                };

                let tempUID = waya.genId();

                console.log('Awaiting db act');
                waya.awaitIDResponse(tempUID).then(done => {
                    console.log('Value', done);

                    waya.smile(tempUID, id_info).then(smile_response => {
                        let js = JSON.parse(smile_response);
                        console.log('Smile response...');
                        console.log(js);
                        if (js.ResultCode == 1012) {

                            //verification Worked, add verification status to body
                            req.body.verified = true;

                            res.send(js.ResultCode);
                        } else {

                            //verification Failed, add verification status to body
                            req.body.verified = false;

                            res.json(smile_response);
                        }
                        console.log(req.body);
                    }, error => {
                        //verification Failed
                        req.body.verified = false;
                        console.log('PromiseError:', error);
                        // console.log(req.body);
                    });
                });

                //Process signup
                // waya.postCall(JSON.stringify(req.body), 'merchant/signup', '').then((resp) => {

                //     //Send Response to Client
                //     waya.out(res, resp, '');
                // });
            }
        // }, err => {
        //     console.log('ERR: ' + err);
        // });
    // });

    // if (fileKeys.length == 0) {
    //     waya.talk(res, 400, { success : false, message : 'No file uploaded' }, true);
    // }
});

route.get('/list', vfy, function(req, res) {
    waya.getCall('', 'merchant/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});


// Wallet
route.post('/addBalance/:merchant/:balType', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), `merchant/${req.params.merchant}/balance/${req.params.balType}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.get('/getBalance/:merchant/:balType', vfy, (req, res) => {
    waya.getCall('', `merchant/${req.params.merchant}/balance/${req.params.balType}`, req.headers['x-access-token']).then((resp) => {
        waya.out(res, resp, '');
    });
});

// Staff
route.post('/createStaff', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'merchant/staff', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getStaff/:staffID', vfy, function(req, res) {
    waya.getCall('', `merchant/staff/${req.params.staffID}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/saveStaff/:staffID', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `merchant/staff/${req.params.staffID}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.delete('/deleteStaff/:staffID', vfy, function(req, res) {
    waya.delCall('', `merchant/staff/${req.params.staffID}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/listStaff', vfy, function(req, res) {
    waya.getCall('', 'merchant/staff/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});


// SettlementAccounts
route.post('/createSettlementAccount', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'merchant/settlementaccount', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/defaultSettlementAccount/:countryCode/:currencyCode', vfy, function(req, res) {
    waya.getCall('', `merchant/settlementaccount?countryCode=${req.params.countryCode}&currencyCode=${req.params.currencyCode}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/settlementAccounts', vfy, function(req, res) {
    waya.getCall('', 'merchant/settlementaccount/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

// Card
route.post('/createCardSettlementAccount', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'merchant/settlementaccount/card', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getCardSettlementAccount/:id', vfy, function(req, res) {
    waya.getCall('', `merchant/settlementaccount/card/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/updateCardSettlementAccount/:id', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `merchant/settlementaccount/card/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.delete('/deleteCardSettlementAccount/:id', vfy, function(req, res) {
    waya.delCall('', `merchant/settlementaccount/card/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getCardSettlementAccounts', vfy, function(req, res) {
    waya.getCall('', 'merchant/settlementaccount/card/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

// Bank
route.post('/createBankSettlementAccount', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'merchant/settlementaccount/bank', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getBankSettlementAccount/:id', vfy, function(req, res) {
    waya.getCall('', `merchant/settlementaccount/bank/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/updateBankSettlementAccount/:id', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `merchant/settlementaccount/bank/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.delete('/deleteBankSettlementAccount/:id', vfy, function(req, res) {
    waya.delCall('', `merchant/settlementaccount/bank/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getBankSettlementAccounts', vfy, function(req, res) {
    waya.getCall('', 'merchant/settlementaccount/bank/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

// Momo
route.post('/createMomoSettlementAccount', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'merchant/settlementaccount/mobilemoney', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getMomoSettlementAccount/:id', vfy, function(req, res) {
    waya.getCall('', `merchant/settlementaccount/mobilemoney/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/updateMomoSettlementAccount/:id', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `merchant/settlementaccount/mobilemoney/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.delete('/deleteMomoSettlementAccount/:id', vfy, function(req, res) {
    waya.delCall('', `merchant/settlementaccount/mobilemoney/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getMomoSettlementAccounts', vfy, function(req, res) {
    waya.getCall('', 'merchant/settlementaccount/mobilemoney/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.get('/settlements', vfy, function(req, res) {
    waya.getCall('', 'merchant/settlements', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});


//Branches
route.post('/createBranch', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'merchant/branch', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/branches', vfy, function(req, res) {
    waya.getCall('', 'merchant/branch/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getBranch/:branchId', vfy, function(req, res) {
    waya.getCall('', `merchant/branch/${req.params.branchId}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/updateBranch/:branchId', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `merchant/branch/${req.params.branchId}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.delete('/deleteBranch/:branchId', vfy, function(req, res) {
    waya.delCall('', `merchant/branch/${req.params.branchId}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});


//Roles
route.post('/createRole', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'merchant/group', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getRole/:roleID', vfy, function(req, res) {
    waya.getCall('', `merchant/group/${req.params.roleID}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.delete('/deleteRole/:roleID', vfy, function(req, res) {
    waya.delCall('', `merchant/group/${req.params.roleID}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/roles', vfy, function(req, res) {
    waya.getCall('', 'merchant/group/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/setRoleMembers/:roleID', vfy, (req, res) => {
    waya.putCall(JSON.stringify(req.body), `merchant/group/${req.params.roleID}/members`, req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/addRoleMembers/:roleID', vfy, (req, res) => {
    waya.putCall(JSON.stringify(req.body), `merchant/group/${req.params.roleID}/members/add`, req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/removeRoleMembers/:roleID', vfy, (req, res) => {
    waya.putCall(JSON.stringify(req.body), `merchant/group/${req.params.roleID}/members/remove`, req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/roleMembers/:roleID', vfy, function(req, res) {
    waya.getCall('', `merchant/group/${req.params.roleID}/members`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});


//Permissions
route.put('/assignRolePermissions/:roleID', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `merchant/group/${req.params.roleID}/permissions`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/addRolePermissions/:roleID', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `merchant/group/${req.params.roleID}/permissions/add`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/removeRolePermissions/:roleID', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `merchant/group/${req.params.roleID}/permissions/remove`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/rolePermissions/:roleID', vfy, function(req, res) {
    waya.getCall('', `merchant/group/${req.params.roleID}/permissions`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});


//API 
route.post('/createAPIKey', vfy, function(req, res) {
    waya.postCall(JSON.stringify(req.body), `merchant/api`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getAPIKey/:merchantID', vfy, function(req, res) {
    waya.getCall('', `merchant/api?merchant=${req.params.merchantID}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.delete('/deleteAPIKey/:merchantID', vfy, function(req, res) {
    waya.delCall(JSON.stringify(req.body), `merchant/api?merchant=${req.params.merchantID}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.post('/keys', function(req, res) {
    assignKeys(req.body['merch']).then((done) => {
        waya.out(res, {'message' : done}, '');
    }, (wrong) => {
        waya.out(res, {'message' : wrong}, '');
    });
});
function assignKeys(merchant) {
    return new Promise(function(resolve, reject) {
        db.pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                conn.release;
            } else {
                var test_key = waya.genId();
                var que = `insert into app_keys (merchant_id, app_key, key_type) values ('${merchant}', '${test_key}', 'test')`;
                conn.query(que, (err) => {
                    conn.release;
                    if (!err) {
                        console.log('No error');
                        resolve(true);
                    } else {
                        console.log(err.code);
                        console.log(err.message);
                        reject(err.message);
                    }
                });
            }
        });
    });
}

module.exports = route;