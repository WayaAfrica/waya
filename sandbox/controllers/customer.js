var waya = require('../controllers/helper');
var vfy = require('../authentication/verify');
var express = require('express');
var route = express();
var bod = require('body-parser');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());

route.post('/create', (req, res) => {
    // console.log(req.body);
    waya.postCall(JSON.stringify(req.body), 'merchant/signup', '').then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/list', vfy, function(req, res) {
    waya.getCall('', 'merchant/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

// Accounts
route.post('/createAccount', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'customer/account', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/Accounts', vfy, function(req, res) {
    waya.getCall('', 'customer/account/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

// Card
route.post('/createCardAccount', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'customer/account/card', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getCardAccount/:id', vfy, function(req, res) {
    waya.getCall('', `customer/account/card/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/updateCardAccount/:id', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `customer/account/card/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.delete('/deleteCardAccount/:id', vfy, function(req, res) {
    waya.delCall('', `customer/account/card/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getCardAccounts', vfy, function(req, res) {
    waya.getCall('', 'customer/account/card/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

// Bank
route.post('/createBankAccount', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'customer/account/bank', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getBankAccount/:id', vfy, function(req, res) {
    waya.getCall('', `customer/account/bank/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/updateBankAccount/:id', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `customer/account/bank/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.delete('/deleteBankAccount/:id', vfy, function(req, res) {
    waya.delCall('', `customer/account/bank/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getBankAccounts', vfy, function(req, res) {
    waya.getCall('', 'customer/account/bank/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

// Momo
route.post('/createMomoAccount', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), 'customer/account/mobilemoney', req.headers['x-access-token']).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getMomoAccount/:id', vfy, function(req, res) {
    waya.getCall('', `customer/account/mobilemoney/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.put('/updateMomoAccount/:id', vfy, function(req, res) {
    waya.putCall(JSON.stringify(req.body), `customer/account/mobilemoney/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.delete('/deleteMomoAccount/:id', vfy, function(req, res) {
    waya.delCall('', `customer/account/mobilemoney/${req.params.id}`, req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});
route.get('/getMomoAccounts', vfy, function(req, res) {
    waya.getCall('', 'customer/account/mobilemoney/list', req.headers['x-access-token']).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

module.exports = route;