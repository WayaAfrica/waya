var waya = require('../controllers/helper');
var vfy = require('../authentication/verify');
var express = require('express');
var route = express();
var bod = require('body-parser');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());

route.post('/create', vfy, (req, res) => {
    waya.postCall(JSON.stringify(req.body), `bank/create`, req.headers['x-access-token']).then((resp) => {
        waya.out(res, resp, '');
    });
});

route.get('/list', vfy, (req, res) => {
    waya.getCall('', `bank/list`, req.headers['x-access-token']).then((resp) => {
        waya.out(res, resp, '');
    });
});

route.get('/list/:country/:partner', vfy, (req, res) => {
    waya.getCall('', `bank/list/${req.params.country}/${req.params.partner}`, req.headers['x-access-token']).then((resp) => {
        waya.out(res, resp, '');
    });
});

module.exports = route;