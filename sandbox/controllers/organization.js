var waya = require('../controllers/helper');
var vfy = require('../authentication/verify');
var express = require('express');
var route = express();
var bod = require('body-parser');
route.use(bod.urlencoded({ extended: true }));
route.use(bod.json());

route.get('/orgunits', vfy, function(req, res) {
    waya.getCall('', 'orgunit/orgunits/getOrganizationUnits', req.headers['x-access-token']).then((resp) => {

        //Check error
        if (resp.status == 500 || resp.status == 403) {
            waya.talk(res, 401, { success:false, response: `An error occurred. ${resp.message}`}, true);
            return false;
        }

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

route.get('/orgunitgroups', vfy, function(req, res) {
    waya.getCall('', 'orgunit/orgunitgroup/all', req.headers['x-access-token']).then((resp) => {

        //Check error
        if (resp.status == 500 || resp.status == 403) {
            waya.talk(res, 401, { success:false, response: `An error occurred. ${resp.message}`}, true);
            return false;
        }

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

module.exports = route;