var db = require('./db');
var waya = require('../controllers/helper');
var express = require('express');
var app = express();
var comms = require('request');
var bod = require('body-parser');
app.use(bod.urlencoded({ extended: true }));
app.use(bod.json());
var path = require('path');
var token;

app.get('/swg', (req, res) => {
    res.sendFile(path.join(__dirname + '/../api/swagger.json'));
})
app.get('/docs', (req, res) => {
    res.sendFile(path.join(__dirname + '/../api/docs.html'));
})

app.get('/getToken', (req, res) => {
    var bearer = req.headers['authorization'];
    if (!bearer) return res.status(403).send({ success: false, message: 'No authorization key provided.' });
    let data = bearer.substr(bearer.indexOf(' ')).trim();
    let buf = Buffer.from(data, 'base64');
    let u_p = buf.toString('ascii');
    var un = u_p.substr(0, u_p.indexOf(':'));
    var pw = u_p.substr(u_p.indexOf(':') + 1);
    login({'username':un, 'password':pw})
})

app.get('/transactionStatus/:trx_id', merchVerify, function(req, res) {
    serve('', `transaction/${req.params.trx_id}`, req.query.live, false).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

app.get('/searchTransaction', merchVerify, function(req, res) {
    serve('', `transaction/${req.params.trx_id}`, req.query.live, false).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

app.post('/requestPayment', merchVerify, function(req, res) {
    serve(JSON.stringify(req.body), `transaction/payment`, req.query.live, true).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

app.post('/transfer/toMomo', merchVerify, function(req, res) {
    if (req.body.destination.accountType !== 'MobileWallet') {
        waya.talk(res, 401, { success:false, response: `Source or detination account types not accurate`}, true);
        return false;
    }
    
    serve(JSON.stringify(req.body), `transaction/transfer`, req.query.live, true).then((resp) => {

        //Send Response to Client
        waya.out(res, resp, '');
    });
});

app.post('/transfer/toBank', merchVerify, function(req, res) {
    if (req.body.destination.accountType !== 'BankAccount') {
        waya.talk(res, 401, { success:false, response: `Source or detination account types not accurate`}, true);
        return false;
    }
    serve(JSON.stringify(req.body), `transaction/transfer`, req.query.live, true).then((resp) => {
        
        //Send Response to Client
        waya.out(res, resp, '');
    });
});

function login(payload, live = 'false') {
    serve(JSON.stringify(payload), 'auth/signin', live, true).then((resp) => {
        if (resp.status == 500 || resp.status == 403 || resp.status == 401 || !resp.token) {
          waya.talk(res, 401, { success:false, response: `${resp.message}. Invalid username or password`}, true);
          return false;
        }
        // Save Token
        token = resp.token;
        console.log(`Response: ${resp}`);
    })
}

function serve(data, endPoint, isLive = 'false', isPost = false) {
    options = {
        headers: { 'Content-Type':'application/json', 'Authorization': `Bearer ${token}` },
        url: (isLive == 'true') ? process.env.BASE + endPoint : process.env.TEST + endPoint,
        body: data,
    };
    console.log(JSON.stringify(options));
    return new Promise(function(resolve, reject) {
        if (isPost) {
            comms.post(options, (err, res, body) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`ResponseBody: ${body}`);
                    resolve(JSON.parse(body));
                }
            });
        } else {
            comms.get(options, (err, res, body) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`ResponseBody: ${body}`);
                    resolve(JSON.parse(body));
                }
            });
        }
    });
}

function merchVerify(req, res, next) {
    var access_token = req.headers['x-access-token'];
    if (!access_token) return res.status(403).send({ success: false, message: 'No access token provided.' });
    next();
}

module.exports = app;