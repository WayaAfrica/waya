var sql = require('mysql');

//  live
var pool = sql.createPool({
    connectionLimit: 50,
    user: 'root',
    password: 'R3dA13rts!',
    database: 'waya',
    socketPath: '/cloudsql/authentic-reach:us-central1:wayabase'
    // socketPath: '/cloudsql/authentic-reach:europe-west1:wayabase',
});

//  local
// var pool = sql.createPool({
//     connectionLimit: 50,
//     user: 'root',
//     password: '',
//     database: 'waya',
//     host: 'localhost',
// });

function formatter(query, data_) {
    if (data_ === '') {
        return query;
    }
    refined_query = sql.format(query, [data_]);
    console.log(refined_query);
    return refined_query;
}

function execute(que, payload) {
    return new Promise(function(resolve, reject) {
        pool.getConnection((err, conn) => {
            if (err) {
                conn.release;
                console.error(err);
                reject(err);
            } else {
                conn.query(formatter(que, payload), (err) => {
                    conn.release;
                    if (!err) {
                        resolve({success : true});
                    } else {
                        console.error(err.code);
                        resolve({success: false, message : err.message});
                    }
                });
            }
        });
    });
}

function update(que, payload) {
    return new Promise(function(resolve, reject) {
        pool.getConnection((err, conn) => {
            if (err) {
                conn.release;
                console.error(err);
                reject(err);
            } else {
                conn.query(que, payload, (err) => {
                    conn.release;
                    if (!err) {
                        resolve({success : true});
                    } else {
                        console.error(err.code);
                        resolve({success: false, message : err.message});
                    }
                });
            }
        });
    });
}

function fetch(que, payload = '') {
    return new Promise(function(resolve, reject) {
        pool.getConnection((err, conn) => {
                conn.release;
            if (err) {
                console.error(err);
                reject(err);
            } else {
                conn.query(formatter(que, payload), (err, result) => {
                    conn.release;
                    if (!err) {
                        resolve({success : true, data : result});
                    } else {
                        console.error(err.code);
                        resolve({success: false, message : err.message});
                    }
                });
            }
        });
    });
}

module.exports = {
    'pool' : pool,
    'sql' : sql,
    'format': formatter,
    'execute': execute,
    'update': update,
    'fetch' : fetch
}