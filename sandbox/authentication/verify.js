function verify_token(req, res, next) {
  var token = req.headers['x-access-token'];
  var api_key = req.headers['x-api-key'];
  if (!token) return res.status(403).send({ success: false, message: 'No auth token provided.' });
  if (!api_key) return res.status(403).send({ success: false, message: 'No api key provided.' });
  if (api_key !== 'ergrgerg8273y29738971803ejh12909023u10eu3u2e93') return res.status(500).send({ success: false, message: 'Failed to authenticate api_key.' });
  next();
}
module.exports = verify_token;