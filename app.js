var express = require('express');
var path = require('path');
var app = express();
var note = require('./controllers/notif');

var general = require('./controllers/general');
var s_general = require('./sandbox/controllers/general');

var bank = require('./controllers/bank');
var s_bank = require('./sandbox/controllers/bank');

var user = require('./controllers/user');
var s_user = require('./sandbox/controllers/user');

var api = require('./controllers/api');
var s_api = require('./sandbox/controllers/api');

var merch = require('./controllers/merchant');
var s_merch = require('./sandbox/controllers/merchant');

var customer = require('./controllers/customer');
var s_customer = require('./sandbox/controllers/customer');

var country = require('./controllers/countries');
var s_country = require('./sandbox/controllers/countries');

var currency = require('./controllers/currency');
var s_currency = require('./sandbox/controllers/currency');

var org = require('./controllers/organization');
var s_org = require('./sandbox/controllers/organization');

var trx = require('./controllers/transactions');
var s_trx = require('./sandbox/controllers/transactions');

var fx = require('./controllers/fx');
var s_fx = require('./sandbox/controllers/fx');

var auth = require('./controllers/auth');
var s_auth = require('./sandbox/controllers/auth');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.header("Access-Control-Allow-Headers", "*");
    next();
});

app.use('/notify', note);
app.use('/uploads', express.static('uploadz'));

app.use('/general', general);
app.use('/sandbox/general', s_general);

app.use('/bank', bank);
app.use('/sandbox/bank', s_bank);

app.use('/users', user);
app.use('/sandbox/users', s_user);

app.use('/live', api);
app.use('/sandbox/api', s_api);

app.use('/merchant', merch);
app.use('/sandbox/merchant', s_merch);

app.use('/customer', customer);
app.use('/sandbox/customer', s_customer);

app.use('/countries', country);
app.use('/sandbox/countries', s_country);

app.use('/currency', currency);
app.use('/sandbox/currency', s_currency);

app.use('/org', org);
app.use('/sandbox/org', s_org);

app.use('/trx', trx);
app.use('/sandbox/trx', s_trx);

app.use('/fx', fx);
app.use('/sandbox/fx', s_fx);

app.use('/auth', auth);
app.use('/sandbox/auth', s_auth);
const dotenv = require('dotenv');
dotenv.config();
const port = process.env.PORT || 3500;


app.get('/smile', (req, res) => {
    console.log(req.body);
    res.send('ACK');
})

app.get('/swg', (req, res) => {
    res.sendFile(path.join(__dirname + '/api/swagger.json'));
})

app.get('/docs', (req, res) => {
    res.sendFile(path.join(__dirname + '/api/docs.html'));
})

var server = app.listen(port, function() {
    console.log(`We\'re a go on ${process.env.PORT}`);
});